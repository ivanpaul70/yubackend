package ro.yuhuu.backend.yubackend.service.implementations;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.google.maps.model.LatLng;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ro.yuhuu.backend.yubackend.controller.requests.UpdateInternshipRequestStatus;
import ro.yuhuu.backend.yubackend.exceptions.*;
import ro.yuhuu.backend.yubackend.model.*;
import ro.yuhuu.backend.yubackend.repository.*;
import ro.yuhuu.backend.yubackend.service.CompanyService;
import ro.yuhuu.backend.yubackend.service.MailSender;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

@Service
public class CompanyServiceImpl implements CompanyService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private InternshipRepository internshipRepository;
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private ContactRepository contactRepository;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private ApplicantRepository applicantRepository;
    @Autowired
    private Cloudinary cloudinary;
    @Autowired
    private ViewRepository viewRepository;
    @Autowired
    private InternshipRequestRepository internshipRequestRepository;


    @Override
    public Photo getCompanyPhoto(Long companyId) throws NotValidContactException, NotValidPhotoException {

        Contact contact = findContactByCompanyId(companyId);
        Photo photo = findPhotoByContactId(contact.getId());

        if (photo.isDeleted() == true) {
            throw new NotValidPhotoException("There is no photo uploaded.");
        }

        return photo;

    }

    private Contact findContactByCompanyId(Long companyId) throws NotValidContactException {
        Optional<Contact> contactOptional = contactRepository.findByCompanyId(companyId);

        if (!contactOptional.isPresent()) {
            throw new NotValidContactException("Error when trying to get the contact from the company with the ID " + companyId);
        }

        Contact contact = contactOptional.get();

        return contact;
    }

    private Photo findPhotoByContactId(Long contactId) throws NotValidContactException {
        Optional<Photo> photoOptional = photoRepository.findPhotoByContact_Id(contactId);

        if (!photoOptional.isPresent()) {
            throw new NotValidContactException("Error when trying to get the photo from the contact id:  " + contactId);
        }

        Photo photo = photoOptional.get();

        return photo;
    }


    @Override
    public Photo deleteCompanyPhoto(Long companyId) throws NotValidContactException, NotAllowedDeletingPhotoException {

        Contact contact = findContactByCompanyId(companyId);
        Photo photo = findPhotoByContactId(contact.getId());

        try {
            cloudinary.uploader().destroy(photo.getPublic_id(),
                    ObjectUtils.emptyMap());

            photo.setDeleted(true);
            photoRepository.saveAndFlush(photo);
        } catch (IOException e) {
            throw new NotAllowedDeletingPhotoException("Error when trying to delete photo");
        }

        return photo;

    }

    @Override
    public List<Company> getSortedCompaniesByNoEmployees(Integer nrItems) {
        List<Company> companyList = companyRepository.findAll();
        Collections.sort(companyList, new Comparator<Company>() {
            @Override
            public int compare(Company o1, Company o2) {
                if (o1.getDimension() == o2.getDimension())
                    return 0;
                return o1.getDimension() < o2.getDimension() ? 1 : -1;
            }
        });
        Integer actualNrOfItems = companyList.size();
        if (actualNrOfItems < nrItems)
            nrItems = actualNrOfItems;
        return (List<Company>) companyList.subList(0, nrItems);
    }

    @Override
    public List<InternshipRequest> getAllInternshipRequestsForCompany() {
        User user = getAuthenticatedUser();
        Company company = user.getCompany();

        Set<InternshipRequest> internshipRequestsForCompany = new HashSet<>();
        List<InternshipRequest> allInternshipRequests = internshipRequestRepository.findAll();
        for (InternshipRequest internshipRequest : allInternshipRequests) {
            if (internshipRequest.getInternship().getCompany().getId() == company.getId())
                internshipRequestsForCompany.add(internshipRequest);
        }

        List<InternshipRequest> sortedList = new ArrayList<>(internshipRequestsForCompany);

        Collections.sort(sortedList, new Comparator<InternshipRequest>() {
            @Override
            public int compare(InternshipRequest o1, InternshipRequest o2) {
                if (o1.getId().equals(o2.getId()))
                    return 0;
                return o1.getId() < o2.getId() ? -1 : 1;
            }
        });
        return sortedList;
    }

    @Override
    public Boolean updateInternshipRequestStatus(Long internshipRequestId, UpdateInternshipRequestStatus updateInternshipRequestStatus) throws NotValidInternshipRequestException, NotAllowedCompanyException {
        InternshipRequest internshipRequest = findInternshipRequestById(internshipRequestId);
        Applicant applicant = internshipRequest.getApplicant();
        Internship internship = internshipRequest.getInternship();

        User user = getAuthenticatedUser();
        Company loggedCompany = user.getCompany();
        Company currentCompany = internship.getCompany();

        if (!(loggedCompany.getId() == currentCompany.getId()))
            throw new NotAllowedCompanyException("You are not allowed to modify this data!");

        String destination = applicant.getUser().getEmail();
        String content = updateInternshipRequestStatus.getContent();
        String subject = updateInternshipRequestStatus.getSubject();

        InternshipRequestStatus internshipRequestStatus = updateInternshipRequestStatus.getInternshipRequestStatus();

        internshipRequest.setInternshipRequestStatus(internshipRequestStatus);
        internshipRequestRepository.save(internshipRequest);
        sendEmail(destination, subject, content);

        return Boolean.valueOf(true);
    }

    private InternshipRequest findInternshipRequestById(Long internshipRequestId) throws NotValidInternshipRequestException {
        Optional<InternshipRequest> optionalInternshipRequest = internshipRequestRepository.findById(internshipRequestId);
        if (!optionalInternshipRequest.isPresent())
            throw new NotValidInternshipRequestException("InternshipRequest with ID:" + internshipRequestId + " doesn't exist!");
        return optionalInternshipRequest.get();
    }

    @Override
    public boolean createCompanyPhotoIfNotExist(Long companyId) throws NotValidContactException {
        boolean created = false;

        Contact contact = findContactByCompanyId(companyId);

        Optional<Photo> photoOptional = photoRepository.findPhotoByContact_Id(contact.getId());

        if (!photoOptional.isPresent()) {
            Photo newPhoto = new Photo();
            contact.setPhoto(newPhoto);
            photoRepository.saveAndFlush(newPhoto);
            created = true;
        }
        return created;
    }


    @Override
    public Photo uploadPhotoForComapny(Long companyId, MultipartFile photoFile) throws NotValidCompanyException, NotAllowedApplicantException, NotValidContactException, NotValidImageUploadException {

        Company company = findCompanyById(companyId);

        if (!checkTheUser(company.getUser())) {
            throw new NotAllowedApplicantException("You don't have permissions to modify data!");
        }

        Contact contact = findContactByCompanyId(companyId);
        Photo photo = findPhotoByContactId(contact.getId());

        try {
            Map uploadResult = cloudinary.uploader().upload(photoFile.getBytes(), ObjectUtils.emptyMap());

            String photoUrl = (String) uploadResult.get("url");
            String public_id = (String) uploadResult.get("public_id");
            photo.setUrl(photoUrl);
            photo.setPublic_id(public_id);
            photo.setDeleted(false);

        } catch (IOException e) {
            throw new NotValidImageUploadException("Error when trying to upload the image");
        }

        photoRepository.saveAndFlush(photo);

        return photo;
    }

    @Override
    public Company getCompanyDetailsById(Long id) throws NotValidCompanyException, NotAllowedCompanyException {
        Company actualCompany = findCompanyById(id);

        return actualCompany;
    }

    private Company findCompanyById(Long id) throws NotValidCompanyException {
        Optional<Company> companyResult = companyRepository.findById(id);
        if (!companyResult.isPresent()) {
            throw new NotValidCompanyException("Company with ID:" + id + " doesn't exist!");
        }

        Company company = companyResult.get();
        return company;
    }

    private boolean checkTheUser(User user) {
        User authenticatedUser = getAuthenticatedUser();
        if (user != null && authenticatedUser != null)
            return authenticatedUser.getId() == user.getId();
        return false;
    }

    @Override
    public Company updateCompanyContactAndAddress(Long companyId, Company newCompany) throws NotValidCompanyException, NotAllowedApplicantException, NotValidContactException {

        Company company = findCompanyById(companyId);

        if (!checkTheUser(company.getUser())) {
            throw new NotAllowedApplicantException("You don't have permissions to update!");
        }

        Contact contact = findContactByCompanyId(companyId);

        updateCompanyContact(contact, newCompany.getContact());
        updateCompanyAddress(contact.getAddress(), newCompany.getContact().getAddress());

        company = findCompanyById(companyId);
        return company;
    }

    private void updateCompanyContact(Contact contact, Contact newContact) {

        initializeNullValuesForContact(contact);

        updateLinkedInLink(contact, newContact);
        updateWebsite(contact, newContact);
        updatePhoneNumber(contact, newContact);
        updateFacebookLink(contact, newContact);

        contactRepository.saveAndFlush(contact);
    }

    private void updateLinkedInLink(Contact currentContact, Contact contact) {
        if (contact.getLinkedinLink() != null && !currentContact.getLinkedinLink().equals(contact.getLinkedinLink()))
            currentContact.setLinkedinLink(contact.getLinkedinLink());
    }

    private void updateWebsite(Contact currentContact, Contact contact) {
        if (contact.getWebsite() != null && !currentContact.getWebsite().equals(contact.getWebsite()))
            currentContact.setWebsite(contact.getWebsite());
    }

    private void updatePhoneNumber(Contact currentContact, Contact contact) {
        if (contact.getPhoneNumber() != null && !currentContact.getPhoneNumber().equals(contact.getPhoneNumber()))
            currentContact.setPhoneNumber(contact.getPhoneNumber());
    }

    private void updateFacebookLink(Contact currentContact, Contact contact) {
        if (contact.getFacebookLink() != null && !currentContact.getFacebookLink().equals(contact.getFacebookLink()))
            currentContact.setFacebookLink(contact.getFacebookLink());
    }


    private void initializeNullValuesForContact(Contact contact) {
        if (contact.getFacebookLink() == null)
            contact.setFacebookLink("");
        if (contact.getPhoneNumber() == null)
            contact.setPhoneNumber("");
        if (contact.getLinkedinLink() == null)
            contact.setLinkedinLink("");
        if (contact.getWebsite() == null)
            contact.setWebsite("");
    }

    private void updateAddress(Address currentAddress, Address address) {
        initializeNullValuesForAddress(currentAddress);

        if (address != null) {
            updateNumber(currentAddress, address);
            updateFloor(currentAddress, address);
            updateCounty(currentAddress, address);
            updateCountry(currentAddress, address);
            updateStreet(currentAddress, address);
            updateSector(currentAddress, address);
            updateBlock(currentAddress, address);
            updateEntrance(currentAddress, address);
            updateApartment(currentAddress, address);
            updateTown(currentAddress, address);
            if (!currentAddress.toString().equals("")) {
                LatLng latLng = getCoordinatesForAddress(currentAddress);
                currentAddress.setLongitude(latLng.lng);
                currentAddress.setLatitude(latLng.lat);
            }
        }
        addressRepository.save(currentAddress);
    }

    private void initializeNullValuesForAddress(Address address) {
        if (address.getNumber() == null)
            address.setNumber("");
        if (address.getApartment() == null)
            address.setApartment("");
        if (address.getBlock() == null)
            address.setBlock("");
        if (address.getCountry() == null)
            address.setCountry("");
        if (address.getCounty() == null)
            address.setCounty("");
        if (address.getEntrance() == null)
            address.setEntrance("");
        if (address.getFloor() == null)
            address.setFloor("");
        if (address.getTown() == null)
            address.setTown("");
        if (address.getSector() == null)
            address.setSector("");
        if (address.getStreet() == null)
            address.setStreet("");
    }

    private LatLng getCoordinatesForAddress(Address address) {
        return Geocoding.get(address.toString());
    }


    private void updateNumber(Address currentAddress, Address address) {
        if (address.getNumber() != null && !currentAddress.getNumber().equals(address.getNumber()))
            currentAddress.setNumber(address.getNumber());
    }

    private void updateApartment(Address currentAddress, Address address) {
        if (address.getApartment() != null && !currentAddress.getApartment().equals(address.getApartment()))
            currentAddress.setApartment(address.getApartment());
    }

    private void updateBlock(Address currentAddress, Address address) {
        if (address.getBlock() != null && !currentAddress.getBlock().equals(address.getBlock()))
            currentAddress.setBlock(address.getBlock());
    }

    private void updateCountry(Address currentAddress, Address address) {
        if (address.getCountry() != null && !currentAddress.getCountry().equals(address.getCountry()))
            currentAddress.setCountry(address.getCountry());
    }

    private void updateCounty(Address currentAddress, Address address) {
        if (address.getCounty() != null && !currentAddress.getCounty().equals(address.getCounty()))
            currentAddress.setCounty(address.getCounty());
    }

    private void updateEntrance(Address currentAddress, Address address) {
        if (address.getEntrance() != null && !currentAddress.getEntrance().equals(address.getEntrance()))
            currentAddress.setEntrance(address.getEntrance());
    }

    private void updateFloor(Address currentAddress, Address address) {
        if (address.getFloor() != null && !currentAddress.getFloor().equals(address.getFloor()))
            currentAddress.setFloor(address.getFloor());
    }

    private void updateTown(Address currentAddress, Address address) {
        if (address.getTown() != null && !currentAddress.getTown().equals(address.getTown()))
            currentAddress.setTown(address.getTown());
    }

    private void updateSector(Address currentAddress, Address address) {
        if (address.getSector() != null && !currentAddress.getSector().equals(address.getSector()))
            currentAddress.setSector(address.getSector());
    }

    private void updateStreet(Address currentAddress, Address address) {
        if (address.getStreet() != null && !currentAddress.getStreet().equals(address.getStreet()))
            currentAddress.setStreet(address.getStreet());
    }


    private void updateCompanyAddress(Address address, Address newAddress) {
        updateAddress(address, newAddress);
        // update Address does the flush by itself
    }


    @Override
    public Company updateCompanyProfile(Long companyId, Company newCompany) throws NotValidCompanyException, NotAllowedApplicantException {

        Company company = findCompanyById(companyId);

        if (!checkTheUser(company.getUser())) {
            throw new NotAllowedApplicantException("You don't have permissions to update!");
        }

        intializeNullValuesForCompanyProfile(company);

        company.setDescription(newCompany.getDescription());
        company.setDimension(newCompany.getDimension());
        company.setName(newCompany.getName());

        companyRepository.saveAndFlush(company);
        return company;

    }

    private void intializeNullValuesForCompanyProfile(Company company) {

        if (company.getDescription() == null) {
            company.setDescription("");
        }

        if (company.getDimension() == null) {
            company.setDimension(new Long(0));
        }

        if (company.getName() == null) {
            company.setName("");
        }

    }

    @Override
    public void incrementViewCounter(Long companyId) throws NotValidCompanyException {

        Company company = findCompanyById(companyId);
        User user = getAuthenticatedUser();

        if (company.getUser().getId() == user.getId())
            return;

        LocalDate currentLocalDate = LocalDate.now();

        Optional<ViewsCounter> optionalView = viewRepository.findViewByUserAndCompany(user, company);

        if (optionalView.isPresent()) {
            ViewsCounter viewsCounter = optionalView.get();
            LocalDate lastViewDate = viewsCounter.getLastViewDay();
            if (!currentLocalDate.equals(lastViewDate)) {
                company.setViews(company.getViews() + 1);
                companyRepository.save(company);
                viewsCounter.setLastViewDay(LocalDate.now());
                viewRepository.save(viewsCounter);
            }
        } else {
            ViewsCounter viewsCounter = new ViewsCounter.Builder()
                    .withUser(user)
                    .withCompany(company)
                    .withLocalDate(currentLocalDate)
                    .build();
            company.setViews(company.getViews() + 1);

            viewRepository.save(viewsCounter);
            companyRepository.save(company);
        }
    }

    @Override
    public Internship removeInternship(Long internshipId) throws NotValidInternshipException, UpdateException {
        Internship internship = findInternshipById(internshipId);
        internshipUpdateChecks(internship);

        Company company = internship.getCompany();
        company.removeInternship(internship);
        companyRepository.saveAndFlush(company);

        return internship;
    }

    private Internship findInternshipById(Long id) throws NotValidInternshipException {
        Optional<Internship> optionalInternship = internshipRepository.findById(id);
        if (!optionalInternship.isPresent())
            throw new NotValidInternshipException("Internship with ID:" + id + " doesn't exist!");

        Internship internship = optionalInternship.get();
        return internship;
    }

    private void internshipUpdateChecks(Internship currentInternship) throws UpdateException {
        User internshipCompanyUser = currentInternship.getCompany().getUser();
        if (!checkTheUser(internshipCompanyUser))
            throw new UpdateException("Not allowed to do this operation!");
    }

    @Override
    public User updateCompanyEmail(Long id, User user) throws NotValidCompanyException, NotAllowedCompanyException, UpdateException {
        Optional<Company> optionalCompany = companyRepository.findById(id);
        if (!optionalCompany.isPresent())
            throw new NotValidCompanyException("Company with ID:" + id + " doesn't exist!");

        Company currentCompany = optionalCompany.get();

        if (!checkTheUser(currentCompany.getUser()))
            throw new NotAllowedCompanyException("You don't have permissions to update!");


        User currentUser = currentCompany.getUser();

        User userWithNewEmail = updateEmail(currentUser, user);
        return userWithNewEmail;
    }

    private User updateEmail(User currentUser, User newUser) throws UpdateException {
        if (currentUser.getEmail().equals(newUser.getEmail()))
            return currentUser;
        if (checkEmailExists(newUser))
            throw new UpdateException("This mail already exists");
        if (newUser.getEmail() != null) {
            currentUser.setEmail(newUser.getEmail());
            return userRepository.save(currentUser);
        }
        return currentUser;
    }


//    @Override
//    public Double getDistanceFromUserToCompany(Long companyId) throws NotValidCompanyException, NotValidAddressException {
//        Optional<Company> optionalCompany = companyRepository.findById(companyId);
//        if (!optionalCompany.isPresent())
//            throw new NotValidCompanyException("Not existing company!");
//
//        Company company = optionalCompany.get();
//        User user = getAuthenticatedUser();
//
//
//        Address firstAddress = company.getContact().getAddress();
//        Address secondAddress = null;
//
//        Set<Role> roles = user.getRoles();
//
//        boolean isApplicant = false, isCompany = false;
//
//        for (Role role : roles) {
//            if (role.getRoleString().toString().equals("APPLICANT")) {
//                isApplicant = true;
//                break;
//            }
//        }
//
//        if (isApplicant) {
//            Optional<Applicant> optionalApplicant = applicantRepository.findApplicantByUser(user);
//            if (optionalApplicant.isPresent()) {
//                Applicant applicant = optionalApplicant.get();
//                secondAddress = applicant.getContact().getAddress();
//            }
//        } else {
//
//            for (Role role : roles) {
//                if (role.getRoleString().toString().equals("COMPANY")) {
//                    isCompany = true;
//                    break;
//                }
//            }
//
//            if (isCompany) {
//                Optional<Company> optionalCompany1 = companyRepository.findCompanyByUser(user);
//                if (optionalCompany.isPresent()) {
//                    Company company1 = optionalCompany1.get();
//                    secondAddress = company1.getContact().getAddress();
//                }
//            }
//        }
//
//        if (secondAddress == null && firstAddress == null)
//            throw new NotValidAddressException("Not valid addresses!");
//        if (firstAddress == null)
//            throw new NotValidAddressException("The company address is not valid!");
//        if (secondAddress == null)
//            throw new NotValidAddressException("Your address is not valid!");
//
//        Double distance = Geocoding.getDistance(firstAddress.toString(), secondAddress.toString());
//        return distance;
//    }

    @Override
    public Double getDistanceFromUserToCompany(Long companyId) throws NotValidCompanyException, NotValidAddressException {
        Optional<Company> optionalCompany = companyRepository.findById(companyId);
        if (!optionalCompany.isPresent())
            throw new NotValidCompanyException("Not existing company!");

        Company company = optionalCompany.get();
        User user = getAuthenticatedUser();


        Address firstAddress = company.getContact().getAddress();
        Address secondAddress = null;


        if (isApplicant()) {
            Optional<Applicant> optionalApplicant = applicantRepository.findApplicantByUser(user);
            if (optionalApplicant.isPresent()) {
                Applicant applicant = optionalApplicant.get();
                secondAddress = applicant.getContact().getAddress();
            }
        } else {
            if (isCompany()) {
                Optional<Company> optionalCompany1 = companyRepository.findCompanyByUser(user);
                if (optionalCompany.isPresent()) {
                    Company company1 = optionalCompany1.get();
                    secondAddress = company1.getContact().getAddress();
                }
            }
        }

        validateAddress(secondAddress, firstAddress);


        Double distance = Geocoding.getDistance(firstAddress.toString(), secondAddress.toString());
        return distance;
    }

    private void validateAddress(Address secondAddress, Address firstAddress) throws NotValidAddressException {
        if (secondAddress == null && firstAddress == null)
            throw new NotValidAddressException("Not valid addresses!");
        if (firstAddress == null)
            throw new NotValidAddressException("The company address is not valid!");
        if (secondAddress == null)
            throw new NotValidAddressException("Your address is not valid!");
    }

    private boolean isApplicant() {
        User user = getAuthenticatedUser();
        Set<Role> roles = user.getRoles();
        for (Role role : roles) {
            if (role.getRoleString().toString().equals("APPLICANT")) {
                return true;
            }
        }
        return false;
    }

    private boolean isCompany() {
        User user = getAuthenticatedUser();
        Set<Role> roles = user.getRoles();
        for (Role role : roles) {
            if (role.getRoleString().toString().equals("COMPANY")) {
                return true;
            }
        }
        return false;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////


    @Override
    public LatLng getCoordinatesForCompany(Long companyId) throws NotValidCompanyException {

        Company company = findCompanyById(companyId);
        Contact contact = company.getContact();
        if (contact != null) {
            Address address = contact.getAddress();
            if (address != null && !(address.toString().equals("")))
                return getCoordinatesForAddress(address);
        }
        return null;
    }

    @Override
    public List<Company> getAllCompanies() {
        return companyRepository.findAll();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public boolean checkEmailExists(User user) {
        return userRepository.findByEmail(user.getEmail()).isPresent();
    }


    public void sendEmail(String destination, String subject, String content) {

        MailSender mailSender = new MailSender(destination, subject, content);
        mailSender.setDaemon(true);
        mailSender.start();
    }

    public User getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String string = (String) authentication.getPrincipal();
        User user = null;
        Optional<User> userUsername = userRepository.findByUsername(string);
        if (!userUsername.isPresent()) {
            Optional<User> userEmail = userRepository.findByEmail(string);
            if (userEmail.isPresent())
                user = userEmail.get();
        } else
            user = userUsername.get();
        return user;
    }

    private Applicant findApplicantById(Long id) throws NotValidApplicantException {

        Optional<Applicant> applicantOptional = applicantRepository.findById(id);
        if (!applicantOptional.isPresent()) {
            throw new NotValidApplicantException("Applicant with ID:" + id + " doesn't exist!");
        }
        Applicant applicant = applicantOptional.get();

        return applicant;

    }

}
