package ro.yuhuu.backend.yubackend.service.implementations;

import com.cloudinary.Cloudinary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ro.yuhuu.backend.yubackend.exceptions.NotAllowedApplicantException;
import ro.yuhuu.backend.yubackend.exceptions.NotValidCommentException;
import ro.yuhuu.backend.yubackend.exceptions.NotValidInternshipException;
import ro.yuhuu.backend.yubackend.model.Comment;
import ro.yuhuu.backend.yubackend.model.Internship;
import ro.yuhuu.backend.yubackend.model.User;
import ro.yuhuu.backend.yubackend.repository.*;
import ro.yuhuu.backend.yubackend.service.CommentService;

import java.util.*;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private InternshipRepository internshipRepository;
    @Autowired
    private CommentRepository commentRepository;

    @Override
    public Comment addComment(Long internshipId, Comment comment) throws NotValidInternshipException {
        Internship internship = findInternshipById(internshipId);

        User user = getAuthenticatedUser();
        Comment resultComment = commentRepository.save(comment);
        user.addComment(resultComment);
        userRepository.save(user);

        internship.addComment(resultComment);
        internshipRepository.save(internship);

        return resultComment;
    }

    @Override
    public Comment addReplyComment(Comment comment, Long parentCommentId) throws NotValidCommentException {
        Comment parent = findCommentById(parentCommentId);
        Comment replyComment = commentRepository.save(comment);

        parent.addSubComment(replyComment);
        commentRepository.save(replyComment);

        User user = getAuthenticatedUser();
        user.addComment(replyComment);
        userRepository.save(user);

        return replyComment;
    }

    @Override
    public List getAllCommentsForInternship(Long internshipId) throws NotValidInternshipException {
        Internship internship = findInternshipById(internshipId);
        Set<Comment> comments = internship.getComments();
        List<Comment> sortedList = new ArrayList<Comment>(comments);
        Collections.sort(sortedList, new Comparator<Comment>() {
            @Override
            public int compare(Comment o1, Comment o2) {
                if (o1.getId().equals(o2.getId()))
                    return 0;
                return o1.getId() < o2.getId() ? -1 : 1;
            }
        });
        return sortedList;
    }

    private Comment findCommentById(Long id) throws NotValidCommentException {
        Optional<Comment> optionalComment = commentRepository.findById(id);
        if (!optionalComment.isPresent())
            throw new NotValidCommentException("Comment with ID:" + id + " doesn't exist!");

        Comment comment = optionalComment.get();
        return comment;
    }


    private Internship findInternshipById(Long id) throws NotValidInternshipException {
        Optional<Internship> optionalInternship = internshipRepository.findById(id);
        if (!optionalInternship.isPresent())
            throw new NotValidInternshipException("Internship with ID:" + id + " doesn't exist!");

        Internship internship = optionalInternship.get();
        return internship;
    }

    @Override
    public Comment removeComment(Long commentId) throws NotValidCommentException, NotAllowedApplicantException {
        Comment comment = findCommentById(commentId);
        if (!checkTheUser(comment.getCreator()))
            throw new NotAllowedApplicantException("Not allowed to remove this comment!");

        Internship internship = comment.getInternship();
        if (internship != null) {
            internship.removeComment(comment);
            commentRepository.delete(comment);
        } else {
            Comment parent = comment.getParent();
            if (parent != null) {
                parent.removeSubComment(comment);
                commentRepository.delete(comment);
            }
        }
        return comment;
    }

    private boolean checkTheUser(User user) {
        User authenticatedUser = getAuthenticatedUser();
        if (user != null && authenticatedUser != null)
            return authenticatedUser.getId() == user.getId();
        return false;
    }

    @Override
    public Comment likeComment(Long commentId) throws NotValidCommentException {

        Comment comment = findCommentById(commentId);
        User user = getAuthenticatedUser();
        Comment resultComent = userLikeComment(comment, user);

        return resultComent;

    }

    @Override
    public Comment dislikeComment(Long commentId) throws NotValidCommentException {
        Comment comment = findCommentById(commentId);
        User user = getAuthenticatedUser();
        Comment resultComent = userDislikeComment(comment, user);
        return resultComent;
    }


    public Comment userLikeComment(Comment comment, User likeningUser) {
        comment.addOrRemoveLike(likeningUser.getId());
        commentRepository.save(comment);
        return comment;
    }

    public Comment userDislikeComment(Comment comment, User dislikingUser) {
        comment.addOrRemoveDislike(dislikingUser.getId());
        commentRepository.save(comment);
        return comment;
    }

    //////////////////////////
    public User getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String string = (String) authentication.getPrincipal();
        User user = null;
        Optional<User> userUsername = userRepository.findByUsername(string);
        if (!userUsername.isPresent()) {
            Optional<User> userEmail = userRepository.findByEmail(string);
            if (userEmail.isPresent())
                user = userEmail.get();
        } else
            user = userUsername.get();
        return user;
    }
}
