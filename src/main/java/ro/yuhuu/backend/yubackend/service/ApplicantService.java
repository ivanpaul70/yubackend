package ro.yuhuu.backend.yubackend.service;

import org.springframework.web.multipart.MultipartFile;
import ro.yuhuu.backend.yubackend.exceptions.*;
import ro.yuhuu.backend.yubackend.model.*;

import java.util.List;
import java.util.Set;

public interface ApplicantService {

    Applicant getApplicantByApplicantId(Long applicantId) throws NotValidApplicantException;

    List<Applicant> getLastXRegisteredApplicants(Long number);

    List<InternshipRequest> getAllInternshipRequestsForApplicant();

    Applicant updateApplicantEducation(Long id,Education education) throws NotValidApplicantException, NotAllowedApplicantException;

    Applicant updateApplicantSkills(Long id, Skill skill) throws NotValidApplicantException, NotAllowedApplicantException, NotValidSkillException;

    Set<Skill> getApplicantSkills(Long id) throws NotValidApplicantException, NotAllowedApplicantException;

    List<Education> getApplicantEducations(Long id) throws NotValidApplicantException, NotAllowedApplicantException;

    Applicant deleteApplicantSkill(Long id,Long skillId) throws NotValidApplicantException, NotAllowedApplicantException, NotValidSkillException;

    Photo uploadPhotoForApplicant(Long id, MultipartFile photoFile) throws NotValidApplicantException, NotAllowedApplicantException, NotValidImageUploadException, NotValidContactException, NotValidPhotoException;

    Photo getApplicantPhoto(Long applicantId) throws NotValidApplicantException, NotAllowedApplicantException, NotValidContactException, NotValidPhotoException;

    Photo deleteApplicantPhoto(Long applicantId) throws NotValidContactException, NotAllowedDeletingPhotoException;

    CV uploadApplicantCV(Long applicantId, MultipartFile cvFile) throws NotValidContactException, NotValidCVException, NotValidCVUploadException;

    CV getApplicantCV(Long applicantId) throws NotValidContactException, NotValidCVException;

    void checkCVIsPDF(String path) throws WrongFormatException;

    CV deleteApplicantCV(Long applicantId) throws NotValidContactException, NotValidCVException, NotAllowedDeletingCVException;

    boolean createApplicantCVIfNotExist(Long applicantId) throws NotValidContactException;


    boolean createApplicantPhotoIfNotExist(Long applicantId) throws NotValidContactException;

    Boolean cancelInternshipRequest(Long internshipRequestId) throws NotValidInternshipRequestException, NotAllowedApplicantException;

    Applicant deleteApplicantEducation(Long id,Long educationId) throws NotValidApplicantException, NotAllowedApplicantException, NotValidEducationException;

    User updateApplicantEmail(Long id, User user) throws NotValidApplicantException, NotAllowedApplicantException, UpdateException;

    Contact updateApplicantContact(Long id,Contact contact) throws NotValidApplicantException, NotAllowedApplicantException;

    Applicant updateApplicant(Long id,Applicant applicant) throws NotValidApplicantException, NotAllowedApplicantException;

}
