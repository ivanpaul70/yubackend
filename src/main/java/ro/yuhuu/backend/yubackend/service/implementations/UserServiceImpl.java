package ro.yuhuu.backend.yubackend.service.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ro.yuhuu.backend.yubackend.exceptions.NotAllowedApplicantException;
import ro.yuhuu.backend.yubackend.exceptions.NotValidApplicantException;
import ro.yuhuu.backend.yubackend.exceptions.NotValidCompanyException;
import ro.yuhuu.backend.yubackend.model.Applicant;
import ro.yuhuu.backend.yubackend.model.Company;
import ro.yuhuu.backend.yubackend.model.User;
import ro.yuhuu.backend.yubackend.repository.UserRepository;
import ro.yuhuu.backend.yubackend.service.UserService;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public User getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String string = (String) authentication.getPrincipal();
        User user = null;
        Optional<User> userUsername = userRepository.findByUsername(string);
        if (!userUsername.isPresent()) {
            Optional<User> userEmail = userRepository.findByEmail(string);
            if (userEmail.isPresent())
                user = userEmail.get();
        } else
            user = userUsername.get();
        return user;
    }

    @Override
    public Company getCompanyByUserId(Long id) throws NotValidCompanyException {
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            if (user.get().getCompany() != null)
                return user.get().getCompany();
        }
        throw new NotValidCompanyException("No company with this user ID!");
    }

    @Override
    public Applicant getApplicantByUserId(Long id) throws NotValidApplicantException, NotAllowedApplicantException {

        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {

            if (!checkTheUser(user.get())) {
                throw new NotAllowedApplicantException("You don't have permissions to remove this data!");
            }

            if (user.get().getApplicant() != null)
                return user.get().getApplicant();
        }
        throw new NotValidApplicantException("No applicant with this user ID!");
    }

    private boolean checkTheUser(User user) {
        User authenticatedUser = getAuthenticatedUser();
        if (user != null && authenticatedUser != null)
            return authenticatedUser.getId() == user.getId();
        return false;
    }
}
