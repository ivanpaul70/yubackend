package ro.yuhuu.backend.yubackend.service;

import ro.yuhuu.backend.yubackend.exceptions.NotAllowedApplicantException;
import ro.yuhuu.backend.yubackend.exceptions.NotValidCommentException;
import ro.yuhuu.backend.yubackend.exceptions.NotValidInternshipException;
import ro.yuhuu.backend.yubackend.model.Comment;

import java.util.List;

public interface CommentService {

    Comment addComment(Long internshipId, Comment comment) throws NotValidInternshipException;

    Comment removeComment(Long commentId) throws NotValidCommentException, NotAllowedApplicantException;

    Comment likeComment(Long commentId) throws NotValidCommentException;

    Comment dislikeComment(Long commentId) throws NotValidCommentException;

    Comment addReplyComment(Comment comment,Long parentCommentId) throws NotValidCommentException;

    List<Comment> getAllCommentsForInternship(Long internshipId) throws NotValidInternshipException;
}
