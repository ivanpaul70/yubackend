package ro.yuhuu.backend.yubackend.service;

import ro.yuhuu.backend.yubackend.model.Skill;

import java.util.List;

public interface SkillService {

    List<Skill> getAllSkills();

}
