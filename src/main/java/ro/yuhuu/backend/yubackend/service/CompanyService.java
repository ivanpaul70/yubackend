package ro.yuhuu.backend.yubackend.service;

import com.google.maps.model.LatLng;
import org.springframework.web.multipart.MultipartFile;
import ro.yuhuu.backend.yubackend.controller.requests.UpdateInternshipRequestStatus;
import ro.yuhuu.backend.yubackend.exceptions.*;
import ro.yuhuu.backend.yubackend.model.*;

import java.util.List;

public interface CompanyService {

    Photo getCompanyPhoto(Long companyId) throws NotValidContactException, NotValidPhotoException;

    Photo deleteCompanyPhoto(Long companyId) throws NotValidContactException, NotAllowedDeletingPhotoException;

    List<Company> getSortedCompaniesByNoEmployees(Integer nrItems);

    List<InternshipRequest> getAllInternshipRequestsForCompany();

    Boolean updateInternshipRequestStatus(Long internshipRequestId, UpdateInternshipRequestStatus updateInternshipRequestStatus) throws NotValidInternshipRequestException, NotAllowedCompanyException;

    boolean createCompanyPhotoIfNotExist(Long companyId) throws NotValidContactException;

    Photo uploadPhotoForComapny(Long companyId, MultipartFile photoFile) throws NotValidCompanyException, NotAllowedApplicantException, NotValidContactException, NotValidImageUploadException;

    Company getCompanyDetailsById(Long id) throws NotValidCompanyException, NotAllowedCompanyException;

    Company updateCompanyContactAndAddress(Long companyId, Company newCompany) throws NotValidCompanyException, NotAllowedApplicantException, NotValidContactException;

    Company updateCompanyProfile(Long companyId, Company newCompany) throws NotValidCompanyException, NotAllowedApplicantException;

    void incrementViewCounter(Long companyId) throws NotValidCompanyException;

    Internship removeInternship(Long internshipId) throws NotValidInternshipException, UpdateException;

    User updateCompanyEmail(Long id, User user) throws NotValidCompanyException, NotAllowedCompanyException, UpdateException;

    Double getDistanceFromUserToCompany(Long companyId) throws NotValidCompanyException, NotValidAddressException;

    LatLng getCoordinatesForCompany(Long companyId) throws NotValidCompanyException;

    List<Company> getAllCompanies();

}
