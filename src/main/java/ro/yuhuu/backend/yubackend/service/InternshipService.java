package ro.yuhuu.backend.yubackend.service;

import ro.yuhuu.backend.yubackend.exceptions.*;
import ro.yuhuu.backend.yubackend.model.*;

import java.util.List;
import java.util.Set;

public interface InternshipService {

    Set<Internship> getAllInternshipsForCompany(Long id) throws NotValidCompanyException, NotAllowedCompanyException;

    Company getCompanyByInternshipId(Long internshipId) throws NotValidInternshipException;

    Internship getInternshipDetailsById(Long id) throws NotValidInternshipException;

    void sendEmail(String destination, String subject, String content);

    Set<Internship> getInternshipsByTags(Set<Tag> tags);

    List<InternshipDTOResponse> getAllInternshipDTOs();

    List<InternshipRequest> getAllInternshipRequestsForSpecificInternship(Long internshipId) throws NotValidInternshipException, NotAllowedCompanyException;

    List<Internship> getAllInternships();

    Photo getInternshipLogo(Long id) throws NotValidInternshipException;

    List<Internship> getLast7DaysInternshipsByTags(Set<Tag> tags);

    Set<Tag> getAllTagsForInternship(Long id) throws NotValidInternshipException;

    Set<Skill> getAllSkillsForInternship(Long id) throws NotValidInternshipException;

    Set<Requirement> getAllRequirementsForInternship(Long id) throws NotValidInternshipException;

    Set<Attribute> getAllAttributesForInternship(Long id) throws NotValidInternshipException;

    Internship updateInternshipSimpleDetails(Long internshipId,Internship internship) throws NotValidInternshipException, UpdateException;

    Tag addTagToInternship(Long internshipId,Tag tag) throws NotValidInternshipException, UpdateException, NotValidTagException;

    Tag removeTagFromInternship(Long internshipId,Long tagId) throws NotValidInternshipException, UpdateException, NotValidTagException;

    Skill addSkillToInternship(Long internshipId,Skill skill) throws NotValidInternshipException, UpdateException, NotValidSkillException;

    Skill removeSkillFromInternship(Long internshipId,Long skillId) throws NotValidInternshipException, UpdateException, NotValidSkillException;

    Requirement addRequirementToInternship(Long internshipId,Requirement requirement) throws NotValidInternshipException, UpdateException;

    Set<Applicant> getApplicantsForSpecificInternship(Long internshipId) throws NotValidInternshipException, NotAllowedCompanyException;

    Requirement removeRequirementFromInternship(Long internshipId,Long requirementId) throws NotValidInternshipException, UpdateException, NotValidRequirementException;

    Attribute addAttributeToInternship(Long internshipId,Attribute attribute) throws NotValidInternshipException, UpdateException;

    InternshipRequest applyForInternship(Long internshipId) throws NotValidInternshipException, NotAllowedApplicantException;

    Attribute removeAttributeFromInternship(Long internshipId,Long attributeId) throws NotValidInternshipException, UpdateException, NotValidAttributeException;

    Internship createInternship(Company company,Internship internship) throws NotValidCompanyException;

}
