package ro.yuhuu.backend.yubackend.service;

import ro.yuhuu.backend.yubackend.exceptions.NotValidCompanyException;
import ro.yuhuu.backend.yubackend.model.Applicant;
import ro.yuhuu.backend.yubackend.model.Company;
import ro.yuhuu.backend.yubackend.model.User;

public interface RegisterService {

    Applicant registerApplicant(User user, Applicant applicant);

    Company registerCompany(User user, Company company) throws NotValidCompanyException;

    boolean checkUsernameExists(User user);

    boolean checkEmailExists(User user);

}
