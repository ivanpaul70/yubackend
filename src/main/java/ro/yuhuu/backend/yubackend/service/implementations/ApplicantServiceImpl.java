package ro.yuhuu.backend.yubackend.service.implementations;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.google.maps.model.LatLng;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ro.yuhuu.backend.yubackend.exceptions.*;
import ro.yuhuu.backend.yubackend.model.*;
import ro.yuhuu.backend.yubackend.repository.*;
import ro.yuhuu.backend.yubackend.service.ApplicantService;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ApplicantServiceImpl implements ApplicantService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private InternshipRepository internshipRepository;
    @Autowired
    private SkillRepository skillRepository;
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private ContactRepository contactRepository;
    @Autowired
    private ApplicantRepository applicantRepository;
    @Autowired
    private EducationRepository educationRepository;
    @Autowired
    private CVRepository cvRepository;
    @Autowired
    private Cloudinary cloudinary;
    @Autowired
    private InternshipRequestRepository internshipRequestRepository;

    @Override
    public Applicant getApplicantByApplicantId(Long applicantId) throws NotValidApplicantException {
        Applicant applicant = findApplicantById(applicantId);

        return applicant;
    }

    private Applicant findApplicantById(Long id) throws NotValidApplicantException {

        Optional<Applicant> applicantOptional = applicantRepository.findById(id);
        if (!applicantOptional.isPresent()) {
            throw new NotValidApplicantException("Applicant with ID:" + id + " doesn't exist!");
        }
        Applicant applicant = applicantOptional.get();

        return applicant;

    }

    @Override
    public List<Applicant> getLastXRegisteredApplicants(Long number) {
        List<Applicant> applicants = applicantRepository.findAll();
        int startIndex = (int) (applicants.size() - number);
        if (startIndex >= 0) {
            return applicants.subList(startIndex, applicants.size());
        }
        return applicants;
    }

    @Override
    public List<InternshipRequest> getAllInternshipRequestsForApplicant() {
        User user = getAuthenticatedUser();
        Applicant applicant = user.getApplicant();

        Set<InternshipRequest> allInternshipRequests = applicant.getInternshipRequests();
        List<InternshipRequest> sortedList = new ArrayList<>(allInternshipRequests);

        Collections.sort(sortedList, new Comparator<InternshipRequest>() {
            @Override
            public int compare(InternshipRequest o1, InternshipRequest o2) {
                if (o1.getId().equals(o2.getId()))
                    return 0;
                return o1.getId() < o2.getId() ? -1 : 1;
            }
        });
        return sortedList;
    }

    @Override
    public Applicant updateApplicantEducation(Long id, Education education) throws NotValidApplicantException, NotAllowedApplicantException {

        Applicant applicant = findApplicantById(id);

        if (!checkTheUser(applicant.getUser()))
            throw new NotAllowedApplicantException("You don't have permissions to modify this data!");

        applicant.addEducation(education);
        return applicantRepository.save(applicant);
    }

    private boolean checkTheUser(User user) {
        User authenticatedUser = getAuthenticatedUser();
        if (user != null && authenticatedUser != null)
            return authenticatedUser.getId() == user.getId();
        return false;
    }

    @Override
    public Applicant updateApplicantSkills(Long id, Skill skill) throws NotValidApplicantException, NotAllowedApplicantException, NotValidSkillException {

        Applicant applicant = findApplicantById(id);

        if (!checkTheUser(applicant.getUser()))
            throw new NotAllowedApplicantException("You don't have permissions to modify this data!");

        Skill skillAux = findSkillByName(skill.getName());
        applicant.addSkill(skillAux);
        applicantRepository.save(applicant);
        return applicant;
    }

    @Override
    public Applicant deleteApplicantSkill(Long id, Long skillId) throws NotValidApplicantException, NotAllowedApplicantException, NotValidSkillException {
        Applicant applicant = findApplicantById(id);

        if (!checkTheUser(applicant.getUser()))
            throw new NotAllowedApplicantException("You don't have permissions to remove this data!");

        Skill skill = findSkillById(skillId);

        applicant.removeSkill(skill);
        return applicantRepository.save(applicant);
    }

    @Override
    public Applicant deleteApplicantEducation(Long id, Long educationId) throws NotValidApplicantException, NotAllowedApplicantException, NotValidEducationException {
        Applicant applicant = findApplicantById(id);

        if (!checkTheUser(applicant.getUser()))
            throw new NotAllowedApplicantException("You don't have permissions to remove this data!");

        Optional<Education> educationOptional = educationRepository.findById(educationId);
        if (!educationOptional.isPresent())
            throw new NotValidEducationException("Education with ID:" + educationId + " doesn't exist!");

        Education education = educationOptional.get();

        applicant.removeEducation(education);
        educationRepository.delete(education);
        return applicant;
    }

    private Skill findSkillById(Long id) throws NotValidSkillException {
        Optional<Skill> skill = skillRepository.findById(id);
        if (!skill.isPresent())
            throw new NotValidSkillException("Skill with ID:" + id + " does not exist!");
        return skill.get();
    }


    private Skill findSkillByName(String name) throws NotValidSkillException {
        Optional<Skill> skill = skillRepository.findByNameEquals(name);
        if (!skill.isPresent())
            throw new NotValidSkillException("Skill with name:" + name + " does not exist!");
        return skill.get();
    }


    @Override
    public Set<Skill> getApplicantSkills(Long id) throws NotValidApplicantException, NotAllowedApplicantException {
        Applicant applicant = findApplicantById(id);
        return applicant.getSkills();
    }

    @Override
    public List<Education> getApplicantEducations(Long id) throws NotValidApplicantException, NotAllowedApplicantException {
        Applicant applicant = findApplicantById(id);

        List<Education> sortedListOfEducationsForAnApplicant = applicant.getEducations().stream().sorted((a, b) -> {

            if (a.getStartDate().isBefore(b.getStartDate())) return 1;
            else if (a.getStartDate().isEqual(b.getStartDate())) return 0;
            return -1;
        }).collect(Collectors.toList());

        return sortedListOfEducationsForAnApplicant;
    }


    @Override
    public Photo uploadPhotoForApplicant(Long applicantId, MultipartFile photoFile) throws NotValidApplicantException, NotAllowedApplicantException, NotValidImageUploadException, NotValidContactException, NotValidPhotoException {

        Applicant applicant = findApplicantById(applicantId);

        if (!checkTheUser(applicant.getUser()))
            throw new NotAllowedApplicantException("You don't have permissions to modify data!");

        Contact contact = findContactByApplicantId(applicantId);
        Photo photo = findPhotoByContactId(contact.getId());

        try {
            Map uploadResult = cloudinary.uploader().upload(photoFile.getBytes(), ObjectUtils.emptyMap());

            String photoUrl = (String) uploadResult.get("url");
            String public_id = (String) uploadResult.get("public_id");
            photo.setUrl(photoUrl);
            photo.setPublic_id(public_id);
            photo.setDeleted(false);

        } catch (IOException e) {
            throw new NotValidImageUploadException("Error when trying to upload the image");
        }

        photoRepository.saveAndFlush(photo);

        return photo;
    }

    @Override
    public Photo getApplicantPhoto(Long applicantId) throws NotValidContactException, NotValidPhotoException {

        Contact contact = findContactByApplicantId(applicantId);
        Photo photo = findPhotoByContactId(contact.getId());

        if (photo.isDeleted() == true) {
            throw new NotValidPhotoException("There is no photo uploaded.");
        }

        return photo;

    }

    @Override
    public Photo deleteApplicantPhoto(Long applicantId) throws NotValidContactException, NotAllowedDeletingPhotoException {

        Contact contact = findContactByApplicantId(applicantId);
        Photo photo = findPhotoByContactId(contact.getId());

        try {
            cloudinary.uploader().destroy(photo.getPublic_id(),
                    ObjectUtils.emptyMap());

            photo.setDeleted(true);
            photoRepository.saveAndFlush(photo);
        } catch (IOException e) {
            throw new NotAllowedDeletingPhotoException("Error when trying to delete photo");
        }

        return photo;


    }

    private Contact findContactByApplicantId(Long applicantId) throws NotValidContactException {
        Optional<Contact> contactOptional = contactRepository.findByApplicantId(applicantId);

        if (!contactOptional.isPresent()) {
            throw new NotValidContactException("Error when trying to get the contact from the  applicant with the ID " + applicantId);
        }

        Contact contact = contactOptional.get();

        return contact;

    }

    private Photo findPhotoByContactId(Long contactId) throws NotValidContactException {
        Optional<Photo> photoOptional = photoRepository.findPhotoByContact_Id(contactId);

        if (!photoOptional.isPresent()) {
            throw new NotValidContactException("Error when trying to get the photo from the contact id:  " + contactId);
        }

        Photo photo = photoOptional.get();

        return photo;
    }

    @Override
    public CV uploadApplicantCV(Long applicantId, MultipartFile cvFile) throws NotValidContactException, NotValidCVException, NotValidCVUploadException {

        Contact contact = findContactByApplicantId(applicantId);
        CV cv = findCVByContactId(contact.getId());

        try {
            Map uploadParams = ObjectUtils.asMap("resource_type", "raw");
            Map uploadResult = cloudinary.uploader().upload(cvFile.getBytes(), uploadParams);

            String CVUrl = (String) uploadResult.get("url");
            String public_id = (String) uploadResult.get("public_id");
            cv.setUrl(CVUrl);
            cv.setPublic_id(public_id);
            cv.setDeleted(false);

        } catch (IOException e) {
//            throw new NotValidCVUploadException(e.getMessage());
            throw new NotValidCVUploadException("Error when trying to upload the CV");
        }

        cvRepository.saveAndFlush(cv);

        return cv;

    }


    private CV findCVByContactId(Long contactId) throws NotValidCVException {

        Optional<CV> cvOptional = cvRepository.findCVByContact_Id(contactId);

        if (!cvOptional.isPresent()) {
            throw new NotValidCVException("Error when trying to get the CV from the contact id:  " + contactId);
        }

        CV cv = cvOptional.get();

        return cv;

    }

    @Override
    public CV getApplicantCV(Long applicantId) throws NotValidContactException, NotValidCVException {
        Contact contact = findContactByApplicantId(applicantId);
        CV cv = findCVByContactId(contact.getId());

        if (cv.isDeleted() == true) {
            throw new NotValidCVException("There is no CV uploaded. Please upload a .pdf format CV");
        }

        return cv;
    }

    @Override
    public void checkCVIsPDF(String path) throws WrongFormatException {
        if (!path.endsWith(".pdf")) {
            throw new WrongFormatException("CV should be in PDF format");
        }

    }


    @Override
    public CV deleteApplicantCV(Long applicantId) throws NotValidContactException, NotValidCVException, NotAllowedDeletingCVException {

        Contact contact = findContactByApplicantId(applicantId);
        CV cv = findCVByContactId(contact.getId());


        try {
            cloudinary.uploader().destroy(cv.getPublic_id(),
                    ObjectUtils.emptyMap());

            cv.setDeleted(true);
            cvRepository.saveAndFlush(cv);
        } catch (IOException e) {
            throw new NotAllowedDeletingCVException("Error when trying to delete CV");
        }

        return cv;


    }


    @Override
    public boolean createApplicantCVIfNotExist(Long applicantId) throws NotValidContactException {

        boolean created = false;

        Contact contact = findContactByApplicantId(applicantId);

        Optional<CV> cvOptional = cvRepository.findCVByContact_Id(contact.getId());

        if (!cvOptional.isPresent()) {
            CV newCV = new CV();
            contact.setCv(newCV);
            cvRepository.saveAndFlush(newCV);
            created = true;
        }
        return created;

    }

    @Override
    public boolean createApplicantPhotoIfNotExist(Long applicantId) throws NotValidContactException {
        boolean created = false;

        Contact contact = findContactByApplicantId(applicantId);

        Optional<Photo> photoOptional = photoRepository.findPhotoByContact_Id(contact.getId());

        if (!photoOptional.isPresent()) {
            Photo newPhoto = new Photo();
            contact.setPhoto(newPhoto);
            photoRepository.saveAndFlush(newPhoto);
            created = true;
        }
        return created;
    }


    @Override
    public Boolean cancelInternshipRequest(Long internshipRequestId) throws NotValidInternshipRequestException, NotAllowedApplicantException {
        InternshipRequest internshipRequest = findInternshipRequestById(internshipRequestId);

        User user = getAuthenticatedUser();
        Applicant applicant = user.getApplicant();

        Applicant internshipRequestApplicant = internshipRequest.getApplicant();
        Internship internship = internshipRequest.getInternship();
        if (!(applicant.getId() == internshipRequestApplicant.getId()))
            throw new NotAllowedApplicantException("You are not allowed to cancel this internship request!");

        applicant.removeInternshipRequest(internshipRequest);
        internship.removeInternshipRequest(internshipRequest);

        internshipRequest.setApplicant(null);
        internshipRequest.setInternship(null);

        applicant.removeInternship(internship);
        internship.removeApplicant(applicant);

        applicantRepository.save(applicant);
        internshipRepository.save(internship);

        internshipRequestRepository.delete(internshipRequest);

        return true;

    }


    private InternshipRequest findInternshipRequestById(Long internshipRequestId) throws NotValidInternshipRequestException {
        Optional<InternshipRequest> optionalInternshipRequest = internshipRequestRepository.findById(internshipRequestId);
        if (!optionalInternshipRequest.isPresent())
            throw new NotValidInternshipRequestException("InternshipRequest with ID:" + internshipRequestId + " doesn't exist!");
        return optionalInternshipRequest.get();
    }


    @Override
    public User updateApplicantEmail(Long id, User user) throws NotValidApplicantException, NotAllowedApplicantException, UpdateException {

        Applicant currentApplicant = findApplicantById(id);

        if (!checkTheUser(currentApplicant.getUser()))
            throw new NotAllowedApplicantException("You don't have permissions to update!");

        User currentUser = currentApplicant.getUser();

        User userWithNewEmail = updateEmail(currentUser, user);
        return userWithNewEmail;
    }


    private User updateEmail(User currentUser, User newUser) throws UpdateException {
        if (currentUser.getEmail().equals(newUser.getEmail()))
            return currentUser;
        if (checkEmailExists(newUser))
            throw new UpdateException("This mail already exists");
        if (newUser.getEmail() != null) {
            currentUser.setEmail(newUser.getEmail());
            return userRepository.save(currentUser);
        }
        return currentUser;
    }

    @Override
    public Contact updateApplicantContact(Long id, Contact contact) throws NotValidApplicantException, NotAllowedApplicantException {

        Applicant currentApplicant = findApplicantById(id);

        if (!checkTheUser(currentApplicant.getUser()))
            throw new NotAllowedApplicantException("You don't have permissions to update!");

        Contact currentContact = currentApplicant.getContact();
        initializeNullValuesForContact(currentContact);

        updateLinkedInLink(currentContact, contact);
        updateWebsite(currentContact, contact);
        updatePhoneNumber(currentContact, contact);
        updateFacebookLink(currentContact, contact);
        updateAddress(currentContact.getAddress(), contact.getAddress());

        return contactRepository.save(currentContact);
    }

    private void initializeNullValuesForContact(Contact contact) {
        if (contact.getFacebookLink() == null)
            contact.setFacebookLink("");
        if (contact.getPhoneNumber() == null)
            contact.setPhoneNumber("");
        if (contact.getLinkedinLink() == null)
            contact.setLinkedinLink("");
        if (contact.getWebsite() == null)
            contact.setWebsite("");
    }

    private void updateLinkedInLink(Contact currentContact, Contact contact) {
        if (contact.getLinkedinLink() != null && !currentContact.getLinkedinLink().equals(contact.getLinkedinLink()))
            currentContact.setLinkedinLink(contact.getLinkedinLink());
    }

    private void updateWebsite(Contact currentContact, Contact contact) {
        if (contact.getWebsite() != null && !currentContact.getWebsite().equals(contact.getWebsite()))
            currentContact.setWebsite(contact.getWebsite());
    }

    private void updatePhoneNumber(Contact currentContact, Contact contact) {
        if (contact.getPhoneNumber() != null && !currentContact.getPhoneNumber().equals(contact.getPhoneNumber()))
            currentContact.setPhoneNumber(contact.getPhoneNumber());
    }

    private void updateFacebookLink(Contact currentContact, Contact contact) {
        if (contact.getFacebookLink() != null && !currentContact.getFacebookLink().equals(contact.getFacebookLink()))
            currentContact.setFacebookLink(contact.getFacebookLink());
    }

    private void updateAddress(Address currentAddress, Address address) {
        initializeNullValuesForAddress(currentAddress);

        if (address != null) {
            updateNumber(currentAddress, address);
            updateFloor(currentAddress, address);
            updateCounty(currentAddress, address);
            updateCountry(currentAddress, address);
            updateStreet(currentAddress, address);
            updateSector(currentAddress, address);
            updateBlock(currentAddress, address);
            updateEntrance(currentAddress, address);
            updateApartment(currentAddress, address);
            updateTown(currentAddress, address);
            if (!currentAddress.toString().equals("")) {
                LatLng latLng = getCoordinatesForAddress(currentAddress);
                currentAddress.setLongitude(latLng.lng);
                currentAddress.setLatitude(latLng.lat);
            }
        }
        addressRepository.save(currentAddress);
    }

    private LatLng getCoordinatesForAddress(Address address) {
        return Geocoding.get(address.toString());
    }

    private void initializeNullValuesForAddress(Address address) {
        if (address.getNumber() == null)
            address.setNumber("");
        if (address.getApartment() == null)
            address.setApartment("");
        if (address.getBlock() == null)
            address.setBlock("");
        if (address.getCountry() == null)
            address.setCountry("");
        if (address.getCounty() == null)
            address.setCounty("");
        if (address.getEntrance() == null)
            address.setEntrance("");
        if (address.getFloor() == null)
            address.setFloor("");
        if (address.getTown() == null)
            address.setTown("");
        if (address.getSector() == null)
            address.setSector("");
        if (address.getStreet() == null)
            address.setStreet("");
    }


    private void updateNumber(Address currentAddress, Address address) {
        if (address.getNumber() != null && !currentAddress.getNumber().equals(address.getNumber()))
            currentAddress.setNumber(address.getNumber());
    }

    private void updateApartment(Address currentAddress, Address address) {
        if (address.getApartment() != null && !currentAddress.getApartment().equals(address.getApartment()))
            currentAddress.setApartment(address.getApartment());
    }

    private void updateBlock(Address currentAddress, Address address) {
        if (address.getBlock() != null && !currentAddress.getBlock().equals(address.getBlock()))
            currentAddress.setBlock(address.getBlock());
    }

    private void updateCountry(Address currentAddress, Address address) {
        if (address.getCountry() != null && !currentAddress.getCountry().equals(address.getCountry()))
            currentAddress.setCountry(address.getCountry());
    }

    private void updateCounty(Address currentAddress, Address address) {
        if (address.getCounty() != null && !currentAddress.getCounty().equals(address.getCounty()))
            currentAddress.setCounty(address.getCounty());
    }

    private void updateEntrance(Address currentAddress, Address address) {
        if (address.getEntrance() != null && !currentAddress.getEntrance().equals(address.getEntrance()))
            currentAddress.setEntrance(address.getEntrance());
    }

    private void updateFloor(Address currentAddress, Address address) {
        if (address.getFloor() != null && !currentAddress.getFloor().equals(address.getFloor()))
            currentAddress.setFloor(address.getFloor());
    }

    private void updateTown(Address currentAddress, Address address) {
        if (address.getTown() != null && !currentAddress.getTown().equals(address.getTown()))
            currentAddress.setTown(address.getTown());
    }

    private void updateSector(Address currentAddress, Address address) {
        if (address.getSector() != null && !currentAddress.getSector().equals(address.getSector()))
            currentAddress.setSector(address.getSector());
    }

    private void updateStreet(Address currentAddress, Address address) {
        if (address.getStreet() != null && !currentAddress.getStreet().equals(address.getStreet()))
            currentAddress.setStreet(address.getStreet());
    }


    @Override
    public Applicant updateApplicant(Long id, Applicant applicant) throws NotValidApplicantException, NotAllowedApplicantException {

        Applicant currentApplicant = findApplicantById(id);

        if (!checkTheUser(currentApplicant.getUser()))
            throw new NotAllowedApplicantException("You don't have permissions to update!");

        if (currentApplicant.getDescription() == null) {
            currentApplicant.setDescription("");
        }

        updateFirstNameApplicant(currentApplicant, applicant);
        updateLastNameApplicant(currentApplicant, applicant);
        updateDescriptionApplicant(currentApplicant, applicant);
        updateBirthdayApplicant(currentApplicant, applicant);

        return applicantRepository.save(currentApplicant);
    }


    private void updateFirstNameApplicant(Applicant currentApplicant, Applicant applicant) {
        if (applicant.getFirstName() != null && !currentApplicant.getFirstName().equals(applicant.getFirstName()))
            currentApplicant.setFirstName(applicant.getFirstName());
    }

    private void updateLastNameApplicant(Applicant currentApplicant, Applicant applicant) {
        if (applicant.getLastName() != null && !currentApplicant.getLastName().equals(applicant.getLastName()))
            currentApplicant.setLastName(applicant.getLastName());
    }

    private void updateDescriptionApplicant(Applicant currentApplicant, Applicant applicant) {
        if (applicant.getDescription() != null && !currentApplicant.getDescription().equals(applicant.getDescription()))
            currentApplicant.setDescription(applicant.getDescription());
    }

    private void updateBirthdayApplicant(Applicant currentApplicant, Applicant applicant) {
        if (applicant.getBirthday() != null && !currentApplicant.getBirthday().equals(applicant.getBirthday()))
            currentApplicant.setBirthday(applicant.getBirthday());
    }


    /////////////////////////////

    public User getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String string = (String) authentication.getPrincipal();
        User user = null;
        Optional<User> userUsername = userRepository.findByUsername(string);
        if (!userUsername.isPresent()) {
            Optional<User> userEmail = userRepository.findByEmail(string);
            if (userEmail.isPresent())
                user = userEmail.get();
        } else
            user = userUsername.get();
        return user;
    }

    public boolean checkEmailExists(User user) {
        return userRepository.findByEmail(user.getEmail()).isPresent();
    }

}
