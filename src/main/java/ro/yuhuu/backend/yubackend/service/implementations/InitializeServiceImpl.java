package ro.yuhuu.backend.yubackend.service.implementations;

import com.cloudinary.Cloudinary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.yuhuu.backend.yubackend.config.SecurityConfig;
import ro.yuhuu.backend.yubackend.model.Applicant;
import ro.yuhuu.backend.yubackend.model.Role;
import ro.yuhuu.backend.yubackend.model.RoleString;
import ro.yuhuu.backend.yubackend.model.User;
import ro.yuhuu.backend.yubackend.repository.*;
import ro.yuhuu.backend.yubackend.service.InitializeService;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.Optional;

@Service
public class InitializeServiceImpl implements InitializeService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private ApplicantRepository applicantRepository;

    @PostConstruct
    private void loadData() {
        initRoles();
        initDatabase();
    }

    private void initRoles() {
        Role roleAdmin = new Role.Builder()
                .withRoleString(RoleString.ADMIN)
                .build();
        roleRepository.save(roleAdmin);
        Role roleApplicant = new Role.Builder()
                .withRoleString(RoleString.APPLICANT)
                .build();
        roleRepository.save(roleApplicant);
        Role roleCompany = new Role.Builder()
                .withRoleString(RoleString.COMPANY)
                .build();
        roleRepository.save(roleCompany);
    }

    private void initDatabase() {
        initUsers();
        //initCompanies();
        //initContactForCompanies();
        //initPhotoForCompanies();
        //initFakeCVForCompanies();
        // initAddressesForCompanies();
        //initTags();
        //initSkills();
        //initAttribute();
        //initRequirements();
        //initInternship();
        initApplicants();
        //addSkillsToApplicant();
        //addApplicantToInternship();
        //initExperience();
        //initEducations();
        //initContactForApplicant();
        // initAddressForApplicant();
        //initPhotoForApplicant();
        //initCVForApplicant();
        //initComments();
    }

    private void initUsers() {
        User userAdmin = new User.Builder()
                .isActive(true)
                .withUsername("admin")
                .withEmail("admin@yuhuu.com")
                .withPassword(SecurityConfig.passwordEncoder().encode("admin"))
                .addRole(roleRepository.findByRoleStringEquals(RoleString.ADMIN))
                .build();
        userRepository.save(userAdmin);
        createApplicantsUser();

    }
    private void createApplicantsUser() {

        User userApplicant = new User.Builder()
                .isActive(true)
                .withUsername("applicant")
                .withEmail("chise_b@yahoo.com")
                .withPassword(SecurityConfig.passwordEncoder().encode("applicant"))
                .addRole(roleRepository.findByRoleStringEquals(RoleString.APPLICANT))
                .build();
        userRepository.save(userApplicant);

        User userApplicant2 = new User.Builder()
                .isActive(true)
                .withUsername("applicant1")
                .withEmail("applicant1@yuhuu.com")
                .withPassword(SecurityConfig.passwordEncoder().encode("applicant1"))
                .addRole(roleRepository.findByRoleStringEquals(RoleString.APPLICANT))
                .build();
        userRepository.save(userApplicant2);

        User userApplicant3 = new User.Builder()
                .isActive(true)
                .withUsername("natalie")
                .withEmail("natalie@yuhuu.com")
                .withPassword(SecurityConfig.passwordEncoder().encode("natalie"))
                .addRole(roleRepository.findByRoleStringEquals(RoleString.APPLICANT))
                .build();
        userRepository.save(userApplicant3);


    }
    private void initApplicants() {
        Applicant applicant = new Applicant.Builder()
                .withFirstName("Chise")
                .withLastName("Bogdan")
                .withBirthday(LocalDate.of(1997, 7, 26))
                .withDescription("Simply clever!!!" +
                        "\nExperienced manager with a demonstrated history of working in the higher education and software development industry. Strong education professional skilled in Agile and Predictive Project Management.")
                .build();
        Optional<User> user = userRepository.findByUsername("applicant");
        user.ifPresent(userAux -> userAux.setApplicant(applicant));
        applicantRepository.saveAndFlush(applicant);

        Applicant applicant1 = new Applicant.Builder()
                .withFirstName("Otniel")
                .withLastName("Boros")
                .withBirthday(LocalDate.of(1997, 1, 1))
                .build();
        Optional<User> user1 = userRepository.findByUsername("applicant1");
        user1.ifPresent(userAux -> userAux.setApplicant(applicant1));
        applicantRepository.saveAndFlush(applicant1);

        Applicant applicant2 = new Applicant.Builder()
                .withFirstName("Natalie")
                .withLastName("Portman")
                .withBirthday(LocalDate.of(1981, 6, 9))
                .build();
        Optional<User> user2 = userRepository.findByUsername("natalie");
        user2.ifPresent(userAux -> userAux.setApplicant(applicant2));
        applicantRepository.saveAndFlush(applicant2);

    }

}
