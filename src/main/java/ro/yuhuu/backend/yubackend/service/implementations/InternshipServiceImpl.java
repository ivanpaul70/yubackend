package ro.yuhuu.backend.yubackend.service.implementations;

import com.cloudinary.Cloudinary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ro.yuhuu.backend.yubackend.exceptions.*;
import ro.yuhuu.backend.yubackend.model.*;
import ro.yuhuu.backend.yubackend.repository.*;
import ro.yuhuu.backend.yubackend.service.InternshipDTOResponse;
import ro.yuhuu.backend.yubackend.service.InternshipService;
import ro.yuhuu.backend.yubackend.service.MailSender;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class InternshipServiceImpl implements InternshipService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private InternshipRepository internshipRepository;
    @Autowired
    private SkillRepository skillRepository;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private RequirementRepository requirementRepository;
    @Autowired
    private AttributeRepository attributeRepository;
    @Autowired
    private InternshipRequestRepository internshipRequestRepository;

    @Override
    public List<Internship> getAllInternships() {
        return internshipRepository.findAll();
    }


    @Override
    public Set<Internship> getAllInternshipsForCompany(Long id) throws NotValidCompanyException, NotAllowedCompanyException {
        Company company1 = findCompanyById(id);
        return company1.getInternships();
    }


    private Company findCompanyById(Long id) throws NotValidCompanyException {
        Optional<Company> companyResult = companyRepository.findById(id);
        if (!companyResult.isPresent()) {
            throw new NotValidCompanyException("Company with ID:" + id + " doesn't exist!");
        }

        Company company = companyResult.get();
        return company;
    }


    @Override
    public Company getCompanyByInternshipId(Long internshipId) throws NotValidInternshipException {
        Internship internship = findInternshipById(internshipId);
        return internship.getCompany();
    }

    @Override
    public void sendEmail(String destination, String subject, String content) {

        MailSender mailSender = new MailSender(destination, subject, content);
        mailSender.setDaemon(true);
        mailSender.start();
    }

    @Override
    public Internship getInternshipDetailsById(Long id) throws NotValidInternshipException {
        Internship internship = findInternshipById(id);
        return internship;
    }

    private Internship findInternshipById(Long id) throws NotValidInternshipException {
        Optional<Internship> optionalInternship = internshipRepository.findById(id);
        if (!optionalInternship.isPresent())
            throw new NotValidInternshipException("Internship with ID:" + id + " doesn't exist!");

        Internship internship = optionalInternship.get();
        return internship;
    }

    @Override
    public Set<Internship> getInternshipsByTags(Set<Tag> tags) {
        Set<Internship> result = new HashSet<>();
        for (Tag tag : tags) {
            Optional<Tag> currentTag = tagRepository.findByNameEquals(tag.getName());
            if (currentTag.isPresent())
                result.addAll(currentTag.get().getInternships());
        }
        return result;
    }

    @Override
    public List<InternshipDTOResponse> getAllInternshipDTOs() {
        List<Internship> internships = getAllInternships();
        List<InternshipDTOResponse> internshipDTOResponses = new ArrayList<>();
        for (Internship internship : internships) {
            internshipDTOResponses.add(new InternshipDTOResponse(internship, internship.getCompany()));
        }
        return internshipDTOResponses;
    }

    @Override
    public List<InternshipRequest> getAllInternshipRequestsForSpecificInternship(Long internshipId) throws NotValidInternshipException, NotAllowedCompanyException {
        User user = getAuthenticatedUser();
        Company company = user.getCompany();
        Internship internship = findInternshipById(internshipId);

        if (!(company.getId() == internship.getCompany().getId()))
            throw new NotAllowedCompanyException("You are not allowed to get this data!");

        Set<InternshipRequest> internshipRequestsForInternship = internship.getInternshipRequests();

        List<InternshipRequest> sortedList = new ArrayList<>(internshipRequestsForInternship);

        Collections.sort(sortedList, new Comparator<InternshipRequest>() {
            @Override
            public int compare(InternshipRequest o1, InternshipRequest o2) {
                if (o1.getInternshipRequestStatus().equals(InternshipRequestStatus.PENDING) && !(o2.getInternshipRequestStatus().equals(InternshipRequestStatus.PENDING))) {
                    return -1;
                } else {
                    if (!(o1.getInternshipRequestStatus().equals(InternshipRequestStatus.PENDING)) && o2.getInternshipRequestStatus().equals(InternshipRequestStatus.PENDING)) {
                        return 1;
                    }
                    return 0;
                }
            }
        });
        return sortedList;
    }

    @Override
    public Photo getInternshipLogo(Long id) throws NotValidInternshipException {
        Internship internship = findInternshipById(id);
        Contact contact = internship.getCompany().getContact();
        return contact.getPhoto();
    }

    @Override
    public List<Internship> getLast7DaysInternshipsByTags(Set<Tag> tags) {

        Set<Internship> internships = getInternshipsByTags(tags);


        List<Internship> resultInternships = internships.stream().filter(
                intern -> intern.getActive() == true &&
                        intern.getStartDate().isAfter(LocalDate.now().minusWeeks(1).minusDays(1))
        )
                .collect(Collectors.toList());

        return resultInternships;

    }

    @Override
    public Set<Tag> getAllTagsForInternship(Long id) throws NotValidInternshipException {
        Internship internship = findInternshipById(id);
        return internship.getTags();
    }

    @Override
    public Set<Skill> getAllSkillsForInternship(Long id) throws NotValidInternshipException {
        Internship internship = findInternshipById(id);
        return internship.getSkills();
    }

    @Override
    public Set<Requirement> getAllRequirementsForInternship(Long id) throws NotValidInternshipException {
        Internship internship = findInternshipById(id);
        return internship.getRequirements();
    }

    @Override
    public Set<Attribute> getAllAttributesForInternship(Long id) throws NotValidInternshipException {
        Internship internship = findInternshipById(id);
        return internship.getAttributes();
    }

    @Override
    public Internship updateInternshipSimpleDetails(Long internshipId, Internship internship) throws NotValidInternshipException, UpdateException {
        Internship currentInternship = findInternshipById(internshipId);
        internshipUpdateChecks(currentInternship);

        return updateInternship(currentInternship, internship);
    }

    private void internshipUpdateChecks(Internship currentInternship) throws UpdateException {
        User internshipCompanyUser = currentInternship.getCompany().getUser();
        if (!checkTheUser(internshipCompanyUser))
            throw new UpdateException("Not allowed to do this operation!");
    }

    private Internship updateInternship(Internship currentInternship, Internship internship) throws UpdateException {
        updateActive(currentInternship, internship);
        updateTitle(currentInternship, internship);
        updateDescriptionInternship(currentInternship, internship);
        updateStartDate(currentInternship, internship);
        updateEndDate(currentInternship, internship);
        updateDeadline(currentInternship, internship);
        updateFreeSpots(currentInternship, internship);
        updateStatus(currentInternship, internship);
        updateEmploymentType(currentInternship, internship);
        return internshipRepository.saveAndFlush(currentInternship);
    }

    private void updateActive(Internship currentInternship, Internship newInternship) {
        if (newInternship.getActive() == null)
            return;
        currentInternship.setActive(newInternship.getActive());
    }

    private void updateTitle(Internship currentInternship, Internship newInternship) {
        if (newInternship.getTitle() == null)
            return;
        currentInternship.setTitle(newInternship.getTitle());
    }

    private void updateDescriptionInternship(Internship currentInternship, Internship newInternship) {
        if (newInternship.getDescription() == null)
            return;
        currentInternship.setDescription(newInternship.getDescription());
    }

    private void updateStartDate(Internship currentInternship, Internship newInternship) throws UpdateException {
        if (newInternship.getStartDate() == null)
            return;
        if (currentInternship.getEndDate() == null)
            currentInternship.setStartDate(newInternship.getStartDate());
        else {
            if (newInternship.getStartDate().isAfter(currentInternship.getEndDate()))
                throw new UpdateException("Start date cannot be after end date!");
            else
                currentInternship.setStartDate(newInternship.getStartDate());
        }
    }

    private void updateEndDate(Internship currentInternship, Internship newInternship) throws UpdateException {
        if (newInternship.getEndDate() == null)
            return;
        if (currentInternship.getStartDate() == null)
            throw new UpdateException("There is no starting date!");
        else {
            if (newInternship.getEndDate().isBefore(currentInternship.getStartDate()))
                throw new UpdateException("End date cannot be before start date!");
            else
                currentInternship.setEndDate(newInternship.getEndDate());
        }
    }

    private void updateDeadline(Internship currentInternship, Internship newInternship) throws UpdateException {
        if (newInternship.getDeadline() == null)
            return;
        if (currentInternship.getStartDate() == null)
            currentInternship.setDeadline(newInternship.getDeadline());
        else {
            if (newInternship.getDeadline().isAfter(currentInternship.getStartDate()))
                throw new UpdateException("Deadline date cannot be after start date!");
            else
                currentInternship.setDeadline(newInternship.getDeadline());
        }
    }

    private void updateFreeSpots(Internship currentInternship, Internship newInternship) throws UpdateException {
        if (newInternship.getFreeSpots() == null)
            return;
        if (newInternship.getFreeSpots() < 0)
            throw new UpdateException("Free spots number cannot be negative!");

        currentInternship.setFreeSpots(newInternship.getFreeSpots());
    }

    private void updateStatus(Internship currentInternship, Internship newInternship) {
        if (newInternship.getStatus() == null)
            return;
        currentInternship.setStatus(newInternship.getStatus());
    }

    private void updateEmploymentType(Internship currentInternship, Internship newInternship) {
        if (newInternship.getEmploymentType() == null)
            return;
        currentInternship.setEmploymentType(newInternship.getEmploymentType());
    }

    private boolean checkTheUser(User user) {
        User authenticatedUser = getAuthenticatedUser();
        if (user != null && authenticatedUser != null)
            return authenticatedUser.getId() == user.getId();
        return false;
    }

    @Override
    public Tag addTagToInternship(Long internshipId, Tag tag) throws NotValidInternshipException, UpdateException, NotValidTagException {
        Internship currentInternship = findInternshipById(internshipId);
        internshipUpdateChecks(currentInternship);
        Tag currentTag = findTagByName(tag.getName());
        currentInternship.addTag(currentTag);

        internshipRepository.saveAndFlush(currentInternship);
        return currentTag;
    }

    private Tag findTagByName(String name) throws NotValidTagException {
        Optional<Tag> tag = tagRepository.findByNameEquals(name);
        if (!tag.isPresent())
            throw new NotValidTagException("Tag with name:" + name + " does not exist!");
        return tag.get();
    }

    @Override
    public Tag removeTagFromInternship(Long internshipId, Long tagId) throws NotValidInternshipException, UpdateException, NotValidTagException {
        Internship currentInternship = findInternshipById(internshipId);
        internshipUpdateChecks(currentInternship);
        Tag currentTag = findTagById(tagId);
        currentInternship.removeTag(currentTag);

        internshipRepository.saveAndFlush(currentInternship);
        return currentTag;
    }

    private Tag findTagById(Long id) throws NotValidTagException {
        Optional<Tag> tag = tagRepository.findById(id);
        if (!tag.isPresent())
            throw new NotValidTagException("Tag with ID:" + id + " does not exist!");
        return tag.get();
    }

    @Override
    public Skill addSkillToInternship(Long internshipId, Skill skill) throws NotValidInternshipException, UpdateException, NotValidSkillException {
        Internship currentInternship = findInternshipById(internshipId);
        internshipUpdateChecks(currentInternship);
        Skill currentSkill = findSkillByName(skill.getName());
        currentInternship.addSkill(currentSkill);

        internshipRepository.saveAndFlush(currentInternship);
        return currentSkill;
    }

    private Skill findSkillByName(String name) throws NotValidSkillException {
        Optional<Skill> skill = skillRepository.findByNameEquals(name);
        if (!skill.isPresent())
            throw new NotValidSkillException("Skill with name:" + name + " does not exist!");
        return skill.get();
    }

    @Override
    public Skill removeSkillFromInternship(Long internshipId, Long skillId) throws NotValidInternshipException, UpdateException, NotValidSkillException {
        Internship currentInternship = findInternshipById(internshipId);
        internshipUpdateChecks(currentInternship);
        Skill currentSkill = findSkillById(skillId);
        currentInternship.removeSkill(currentSkill);

        internshipRepository.saveAndFlush(currentInternship);
        return currentSkill;
    }

    private Skill findSkillById(Long id) throws NotValidSkillException {
        Optional<Skill> skill = skillRepository.findById(id);
        if (!skill.isPresent())
            throw new NotValidSkillException("Skill with ID:" + id + " does not exist!");
        return skill.get();
    }

    @Override
    public Requirement addRequirementToInternship(Long internshipId, Requirement requirement) throws NotValidInternshipException, UpdateException {
        Internship currentInternship = findInternshipById(internshipId);
        internshipUpdateChecks(currentInternship);
        Requirement currentRequirement = requirementRepository.saveAndFlush(requirement);
        currentInternship.addRequirement(currentRequirement);
        internshipRepository.saveAndFlush(currentInternship);
        return currentRequirement;
    }

    @Override
    public Set<Applicant> getApplicantsForSpecificInternship(Long internshipId) throws NotValidInternshipException, NotAllowedCompanyException {
        Internship internship = findInternshipById(internshipId);
        User user = getAuthenticatedUser();
        Company loggedCompany = user.getCompany();
        Company currentCompany = internship.getCompany();
        if (!(currentCompany.getId() == loggedCompany.getId()))
            throw new NotAllowedCompanyException("You can't get this information.");

        return internship.getApplicants();
    }

    @Override
    public Requirement removeRequirementFromInternship(Long internshipId, Long requirementId) throws NotValidInternshipException, UpdateException, NotValidRequirementException {
        Internship currentInternship = findInternshipById(internshipId);
        internshipUpdateChecks(currentInternship);
        Requirement currentRequirement = findRequirementById(requirementId);
        currentInternship.removeRequirement(currentRequirement);

        internshipRepository.saveAndFlush(currentInternship);
        return currentRequirement;
    }

    private Requirement findRequirementById(Long requirementId) throws NotValidRequirementException {
        Optional<Requirement> optionalRequirement = requirementRepository.findById(requirementId);
        if (!optionalRequirement.isPresent())
            throw new NotValidRequirementException("Requirement with ID:" + requirementId + " doesn't exist!");

        Requirement requirement = optionalRequirement.get();
        return requirement;
    }

    @Override
    public Attribute addAttributeToInternship(Long internshipId, Attribute attribute) throws NotValidInternshipException, UpdateException {
        Internship currentInternship = findInternshipById(internshipId);
        internshipUpdateChecks(currentInternship);
        Attribute currentAttribute = attributeRepository.saveAndFlush(attribute);

        currentInternship.addAttribute(currentAttribute);
        internshipRepository.saveAndFlush(currentInternship);
        return currentAttribute;
    }

    @Override
    public InternshipRequest applyForInternship(Long internshipId) throws NotValidInternshipException, NotAllowedApplicantException {
        Internship internship = findInternshipById(internshipId);
        User user = getAuthenticatedUser();

        Applicant applicant = user.getApplicant();

        if (findInternshipRequestByApplicantAndInternship(applicant, internship) != null)
            throw new NotAllowedApplicantException("You can't apply again for this internship!");

        applicant.getInternship().add(internship);
        internship.getApplicants().add(applicant);

        return createInternshipRequest(internship, applicant, InternshipRequestStatus.PENDING);
    }

    private InternshipRequest findInternshipRequestByApplicantAndInternship(Applicant applicant, Internship internship) {
        Optional<InternshipRequest> optionalInternshipRequest = internshipRequestRepository.findInternshipRequestByApplicantAndInternship(applicant, internship);
        if (!optionalInternshipRequest.isPresent())
            return null;
        return optionalInternshipRequest.get();
    }

    private InternshipRequest createInternshipRequest(Internship internship, Applicant applicant, InternshipRequestStatus internshipRequestStatus) {
        InternshipRequest internshipRequest = new InternshipRequest.Builder()
                .withInternship(internship)
                .withApplicant(applicant)
                .withStatus(internshipRequestStatus)
                .withLogoUrl(internship.getCompany().getContact().getPhoto().getUrl())
                .build();
        InternshipRequest createdInternshipRequest = internshipRequestRepository.save(internshipRequest);
        applicant.getInternshipRequests().add(createdInternshipRequest);

        internship.getInternshipRequests().add(createdInternshipRequest);
        internshipRepository.save(internship);
        return createdInternshipRequest;
    }

    @Override
    public Attribute removeAttributeFromInternship(Long internshipId, Long attributeId) throws NotValidInternshipException, UpdateException, NotValidAttributeException {
        Internship currentInternship = findInternshipById(internshipId);
        internshipUpdateChecks(currentInternship);
        Attribute currentAttribute = findAttributeById(attributeId);
        currentInternship.removeAttribute(currentAttribute);

        internshipRepository.saveAndFlush(currentInternship);
        return currentAttribute;
    }

    private Attribute findAttributeById(Long attributeId) throws NotValidAttributeException {
        Optional<Attribute> optionalAttribute = attributeRepository.findById(attributeId);
        if (!optionalAttribute.isPresent())
            throw new NotValidAttributeException("Attribute with ID:" + attributeId + " doesn't exist!");

        Attribute attribute = optionalAttribute.get();
        return attribute;
    }


    @Override
    public Internship createInternship(Company company, Internship internship) throws NotValidCompanyException {
        Company actualComapny = findCompanyById(company.getId());

        actualComapny.addInternship(internship);
        return internshipRepository.saveAndFlush(internship);
    }


    ///////////////////////////
    public User getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String string = (String) authentication.getPrincipal();
        User user = null;
        Optional<User> userUsername = userRepository.findByUsername(string);
        if (!userUsername.isPresent()) {
            Optional<User> userEmail = userRepository.findByEmail(string);
            if (userEmail.isPresent())
                user = userEmail.get();
        } else
            user = userUsername.get();
        return user;
    }
}
