package ro.yuhuu.backend.yubackend.service.implementations;

import com.cloudinary.Cloudinary;
import com.google.maps.model.LatLng;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.yuhuu.backend.yubackend.config.SecurityConfig;
import ro.yuhuu.backend.yubackend.exceptions.NotValidCompanyException;
import ro.yuhuu.backend.yubackend.model.*;
import ro.yuhuu.backend.yubackend.repository.*;
import ro.yuhuu.backend.yubackend.service.RegisterService;

import java.util.HashSet;
import java.util.Set;

@Service
public class RegisterServiceImpl implements RegisterService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private AddressRepository addressRepository;
    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private ContactRepository contactRepository;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private ApplicantRepository applicantRepository;
    @Autowired
    private CVRepository cvRepository;


    @Override
    public Applicant registerApplicant(User user, Applicant applicant) {
        user.setActive(true);
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepository.findByRoleStringEquals(RoleString.APPLICANT));
        user.setRoles(roles);
        user.setPassword(SecurityConfig.passwordEncoder().encode(user.getPassword()));
        User userResult = userRepository.save(user);
        userResult.setApplicant(applicant);
        Applicant currentApplicant = applicantRepository.saveAndFlush(applicant);
        Contact contact = currentApplicant.getContact();
        if (contact == null) {
            initDefaultContactForApplicant(currentApplicant);

            Contact currentContact = currentApplicant.getContact();

            initDefaultAddressForContact(currentContact);
            initDefaulPhotoForApplicant(currentContact);
            initDefaultCVForApplicant(currentContact);
        } else {
            setLatLngForAddress(contact.getAddress());
        }
        if (applicant.getDescription() == null) {
            applicant.setDescription("");
        }
        return currentApplicant;
    }

    private void initDefaultContactForApplicant(Applicant applicant) {
        Contact contact = new Contact.Builder()
                .withFacebookLink("")
                .withPhoneNumber("")
                .withWebsite("")
                .withLinkedinLink("")
                .build();
        applicant.setContact(contact);
        contactRepository.saveAndFlush(contact);
    }

    private void initDefaultAddressForContact(Contact contact) {
        Address address = new Address.Builder()
                .withNumber("")
                .withCountry("")
                .withCounty("")
                .withPostalCode("")
                .withSector("")
                .withStreet("")
                .withEntrance("")
                .withApartment("")
                .withBlock("")
                .withFloor("")
                .withTown("")
                .build();
        contact.setAddress(address);
        addressRepository.saveAndFlush(address);
    }

    private void initDefaultCVForApplicant(Contact contact) {

        CV cv = new CV();
        contact.setCv(cv);
        cvRepository.saveAndFlush(cv);
    }

    private void initDefaulPhotoForApplicant(Contact contact) {

        Photo photo = new Photo();
        contact.setPhoto(photo);

        photoRepository.saveAndFlush(photo);
    }

    @Override
    public Company registerCompany(User user, Company company) throws NotValidCompanyException {
        user.setActive(true);
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepository.findByRoleStringEquals(RoleString.APPLICANT));
        user.setRoles(roles);
        user.setPassword(SecurityConfig.passwordEncoder().encode(user.getPassword()));
        User userResult = userRepository.save(user);
        userResult.setCompany(company);
        Company currentCompany = companyRepository.saveAndFlush(company);
        Contact contact = currentCompany.getContact();

        if (contact != null) {
            setLatLngForAddress(contact.getAddress());
        } else {
            initDefaultContactForCompany(currentCompany);
            initDefaultAddressForCompany(currentCompany.getContact());
            initDefaultCVForCompany(currentCompany);
            initDefaulPhotoForCompany(currentCompany);

        }

        return currentCompany;
    }

    private void initDefaulPhotoForCompany(Company company) {

        Photo photo = new Photo();
        company.getContact().setPhoto(photo);
        photoRepository.saveAndFlush(photo);

    }

    private void initDefaultCVForCompany(Company company) {

        CV cv = new CV();
        company.getContact().setCv(cv);
        cvRepository.saveAndFlush(cv);

    }

    private void initDefaultAddressForCompany(Contact contact) {
        Address address = new Address.Builder()
                .withNumber("")
                .withCountry("")
                .withCounty("")
                .withPostalCode("")
                .withSector("")
                .withStreet("")
                .withEntrance("")
                .withApartment("")
                .withBlock("")
                .withFloor("")
                .withTown("")
                .build();
        contact.setAddress(address);
        addressRepository.saveAndFlush(address);
    }

    private void initDefaultContactForCompany(Company company) {
        Contact contact = new Contact.Builder()
                .withFacebookLink("")
                .withPhoneNumber("")
                .withWebsite("")
                .withLinkedinLink("")
                .build();
        company.setContact(contact);
        contactRepository.saveAndFlush(contact);
    }

    private void setLatLngForAddress(Address address) {
        if (address != null && !address.toString().equals("")) {
            LatLng latLng = getCoordinatesForAddress(address);
            address.setLatitude(latLng.lat);
            address.setLongitude(latLng.lng);
            addressRepository.saveAndFlush(address);
        }
    }

    private LatLng getCoordinatesForAddress(Address address) {
        return Geocoding.get(address.toString());
    }

    @Override
    public boolean checkUsernameExists(User user) {
        return userRepository.findByUsername(user.getUsername()).isPresent();
    }


    @Override
    public boolean checkEmailExists(User user) {
        return userRepository.findByEmail(user.getEmail()).isPresent();
    }


}
