package ro.yuhuu.backend.yubackend.service;

import ro.yuhuu.backend.yubackend.exceptions.NotAllowedApplicantException;
import ro.yuhuu.backend.yubackend.exceptions.NotValidApplicantException;
import ro.yuhuu.backend.yubackend.exceptions.NotValidCompanyException;
import ro.yuhuu.backend.yubackend.model.Applicant;
import ro.yuhuu.backend.yubackend.model.Company;
import ro.yuhuu.backend.yubackend.model.User;

public interface UserService {

    abstract User getAuthenticatedUser();

    Company getCompanyByUserId(Long id) throws NotValidCompanyException;

    Applicant getApplicantByUserId(Long id) throws NotValidApplicantException, NotAllowedApplicantException;
}
