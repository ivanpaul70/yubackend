package ro.yuhuu.backend.yubackend.service.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.yuhuu.backend.yubackend.model.Skill;
import ro.yuhuu.backend.yubackend.repository.SkillRepository;
import ro.yuhuu.backend.yubackend.service.SkillService;

import java.util.List;

@Service
public class SkillServiceImpl implements SkillService {
    @Autowired
    private SkillRepository skillRepository;

    @Override
    public List<Skill> getAllSkills() {
        return skillRepository.findAll();
    }

}
